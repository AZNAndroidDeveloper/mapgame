package uz.azn.mapgame

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Build
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class MapsActivity : AppCompatActivity(), OnMapReadyCallback {

    private var mMap: GoogleMap? = null
    var location: Location? = null
    var me :LatLng? = null
    var pockemon  = ArrayList<Pockemon>()
    var oldLocation :Location?= null
    var myPower = 0.0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        mMap?.let {
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(me,18f))
        }
        checkPermission()
        getPockemonList()
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap!!.uiSettings.isZoomControlsEnabled = true
    }

    fun checkPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(
                    this, android.Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // permission request
                requestPermissions(arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), 2)
                return
            }
        }
        getUserLocation()

    }

    @SuppressLint("MissingPermission")
    fun getUserLocation() {
        val myLocation = MyLocationListener()
        var locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 3, 3f, myLocation)
        val myThread = MyThread()
        myThread.start()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            2 -> {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getUserLocation()
                } else {
                    Toast.makeText(
                        applicationContext,
                        "This permission is required",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    // bu foydalanuvchi turgan  joyni aniqlaydi
    inner class MyLocationListener : LocationListener {

        constructor() {
            oldLocation = Location("Start")
            oldLocation!!.longitude = 0.0
            oldLocation!!.latitude = 0.0
        }

        override fun onLocationChanged(p0: Location) {
            location = p0
        }

        override fun onProviderDisabled(provider: String) {

        }

        override fun onProviderEnabled(provider: String) {

        }

        override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {
            // jps ni ochib yonishini bilsa boladi
        }

    }

    inner class MyThread : Thread {
        constructor() : super() {}

        override fun run() {
            while (true) {
                try {
                    if(oldLocation!!.distanceTo(location) == 0f){
                        continue
                    }
                    oldLocation = location
                    runOnUiThread {
                        mMap!!.clear()
                       me = LatLng(location!!.latitude, location!!.longitude)
                        mMap!!.addMarker(
                            MarkerOptions().position(me!!)
                                .title("Me")
                                .snippet("This my location")
                                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bussiness_man))
                        )


                        for (i in pockemon.indices){
                            val pockemonItem = pockemon[i]
                            if (pockemonItem.isCatch == true){
                                val location = LatLng(pockemonItem.lat!!,pockemonItem.log!!)
                                mMap!!.addMarker(
                                    MarkerOptions()
                                        .position(location)
                                        .title(pockemonItem.name)
                                        .snippet("${pockemonItem.desc}- ${pockemonItem.power}")
                                        .icon(BitmapDescriptorFactory.fromResource(pockemonItem.image!!))
                                )
                                mMap!!.moveCamera(CameraUpdateFactory.newLatLngZoom(me,18f))
                                if(oldLocation!!.distanceTo(pockemonItem.location)<2){
                                    pockemonItem.isCatch = true
                                    pockemon[i] = pockemonItem
                                    myPower += pockemonItem.power!!
                                }


                            }
                        }
                    }
                    sleep(1000)
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            }
        }
    }
    fun getPockemonList(){
        pockemon.add(Pockemon(R.drawable.charmander,"Charmander","This charmander",35.0,41.3591,69.3008))
        pockemon.add(Pockemon(R.drawable.bulbasaur,"Bulbasaur","This bulbasaur",40.0,41.3588,69.2997))
        pockemon.add(Pockemon(R.drawable.squirtle,"Squirtle","This squirtle",27.0,41.3582,69.3005))

    }
}